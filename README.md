# BTRFS Snapshot Rotate

Script to create and rotate BTRFS snapshots
Script to backup a folder and also be able to maintain several previous versions efficiently

## Usage

```
Usage:
  btrfssnapshots [options]

Options:
  -n                Create a new snapshot before rotating
  -d <snapshot dir> Directory of snapshots (default: ~/.snapshots)
  -s <dir>          sync snapshots to other directory
  -h                Show this screen.
```
